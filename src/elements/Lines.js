import React from "react";

const Lines = ({ vertical, color, width, height }) => {
  return (
    <hr
      style={{
        backgroundColor: color,
        width: width,
        height: height,
        marginBottom: vertical,
        marginTop: vertical,
      }}
    />
  );
};

Lines.defaultProps = {
  width: "100%",
  color: "black",
};

export default Lines;
